<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html  xml:lang="it" lang="it">
<title>New tastes</title>
<link rel="icon" href="immagini/favicon.ico" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="login.js"></script>

<style type="text/css">@import url(style.css);</style>
<style type="text/css">@import url(login.css);</style>
<style type="text/css">@import url(admin.css);</style>

<?php session_start();?>

<body>
  <div class="w3-top">
    <!-- barra di navigazione del sito -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="sito.php">New tastes</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" >
      <div id="simb" class="simbolo" onclick="Simbolo()">
      <div class="bar1"></div>
      <div class="bar2"></div>
      <div class="bar3"></div>
      </div>
    </button>
    <div class="collapse navbar-collapse  " id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" onclick="Simbolo()" href="sito.php">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" onclick="Simbolo()" href="sito.php#about">Chi siamo</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" onclick="Simbolo()" href="sito.php#panini">Panini</a>
        </li>
        <li class="nav-item">
          <a class="nav-link"  onclick="Simbolo()" href="sito.php#contact">Contatti</a>
        </li>

        <?php
        if(isset($_SESSION["login"]) ){
            if($_SESSION["login"]== true )
            {
            echo"<li class='nav-item'>
                  <a  id='logout' class='nav-link' onclick='chiusura(2);' name='log' >Logout</a>
                </li>";
              }
            }
              ?>
      </ul>
    </div>
    </nav>
  </div>


<?php
if(isset($_SESSION["msg"]) == TRUE){
    if($_SESSION["msg"]!=""){
      echo"<div id='msgerrore' class='finestralogout' style='display:block;'>
          <!-- Modal content -->
          <div class='modal-content'>
            <span class='close' onclick='chiusuraF(2)'>&times;</span>
            <p>".$_SESSION["msg"]."</p>
          </div>
        </div>";
    }
    $_SESSION["msg"]="";
}

?>

<!--corpo della pagina-->
  <div class="w3-container stringi abbassa" style=" min-height: 67.9%;">

    <div class="w3-row-padding w3-center" >
        <div class="w3-quarter w3-margin-bottom modcel" >
          <button id="btnmodifica" class="btnmodify" style="" ><a href="formmodifica.php">Modifica panino</a></button>
      </div>
      <div class="w3-quarter w3-margin-bottom modcel" >
          <button id="btnaggiungi" class="btnmodify"  style=""><a href="formaggiungi.php">Aggiungi panino</a></button>
     </div>
      <div class="w3-quarter w3-margin-bottom modcel" >
          <button id="btnelimina" class="btnmodify" style="" ><a href="formelimina.php">Elimina panino</a></button>
    </div>
      <div class="w3-quarter w3-margin-bottom modcel">
          <button id="btnordini" class="btnmodify" style=""><a href="formordini.php">Visualizza ordini</a></button>
    </div>
   </div>

  </div>



<!-- riga finale -->
<footer class= "w3-Grey w3-padding-16 footerfisso"  style=" background-color:lightgray">
  <div class="w3-row">
  <div class="w3-third w3-container" >
    <a target="_blank" href=""><p class="sposta w3-left ">&nbsp;Privacy policy&nbsp;</p ></a>
    <a target="_blank" href=""><p class="sposta w3-left ">Cookie policy </p></a>
  </div>
  <div class="w3-third w3-container ">
    <p class="w3-center">P.IVA - C.F 01234567890</p>
  </div>
  <div class="w3-third w3-container">
    <p class="w3-right sposta">Copyright © 2018 New tastes&nbsp;&nbsp;</p>
  </div>
</div>
</footer>

</body>
</html>
