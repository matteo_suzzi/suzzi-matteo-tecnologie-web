var link = document.createElement("link");
link.setAttribute("href","style.css");


/*funzione che viene invocata quando l'utente seleziona il pulsante per registrarsi, cambio il form per aggiungere capi da riempire*/
function notregister(){

  var btn =$("#btnnotregister");
  var psw1=$("#repeat1");
  var psw2=$("#repeat2");
  var privacy=$("#privacy");
  var btnaccedi=$("#btnaccedi");
  var btnregistrati=$("#btnregistrati");
  var btnback=$('#btnback');

  btn.css("display","none");
  psw1.css("display","block");
  psw2.css("display","block");
  psw2.attr("required",true);
  privacy.css("display","block");
  btnaccedi.css("display","none");
  btnregistrati.css("display","block");
  btnback.css("display","block");

}


  // quando clicco logout appare il messaggio
  function chiusura(num) {

    var modal =$("#msglogout");  //finestra a comparsa per Logout
    var btn = $("#logout");  // Get the button that opens the modal
    var acquisto=$("#acquisto");
    var btnacquista=$("#btnacquista");
    var login=$("#login");

      login.css("display","block");
      btn.css("display","none");
      acquisto.css("display","none");
      btnacquista.css("display","none");

    if(num==1){
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.open("POST", "logout.php",true);
          xmlhttp.send("logout");
          modal.css("display","block");
    }
    if(num==2){
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open("POST", "logout.php",true);
      xmlhttp.send("logoutadmin");
      location.href="sito.php";
      }

  }


  // When the user clicks on <span> (x), close the modal
 function chiusuraF(num) {
    if(num==1){
      var modal =$("#msglogout"); //finestra a comparsa per Logout
    }
    if(num==2){
      var modal =$("#msgerrore"); //finestra a comparsa per Logout
    }

    modal.css("display","none");
    location.reload();
  }
