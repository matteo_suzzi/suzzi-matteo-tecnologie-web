<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html  xml:lang="it" lang="it">
<title>New tastes</title>
<link rel="icon" href="immagini/favicon.ico" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="login.js"></script>

<style type="text/css">@import url(style.css);</style>
<style type="text/css">@import url(login.css);</style>

<?php session_start();?>

<body>
  <div class="w3-top">
    <!-- barra di navigazione del sito -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="sito.php">New tastes</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" >
      <div id="simb" class="simbolo" onclick="Simbolo()">
      <div class="bar1"></div>
      <div class="bar2"></div>
      <div class="bar3"></div>
      </div>
    </button>
    <div class="collapse navbar-collapse  " id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" onclick="Simbolo()" href="sito.php">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" onclick="Simbolo()" href="sito.php#about">Chi siamo</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" onclick="Simbolo()" href="sito.php#panini">Panini</a>
        </li>
        <li class="nav-item">
          <a class="nav-link"  onclick="Simbolo()" href="sito.php#contact">Contatti</a>
        </li>
      </ul>
    </div>
    </nav>
  </div>


<?php
if(isset($_SESSION["msg"]) == TRUE){
    if($_SESSION["msg"]!=""){
      echo"<div id='msgerrore' class='finestralogout' style='display:block;'>
          <!-- Modal content -->
          <div class='modal-content'>
            <span class='close' onclick='chiusuraF(2)'>&times;</span>
            <p>".$_SESSION["msg"]."</p>
          </div>
        </div>";
    }
    $_SESSION["msg"]="";
}

?>

<!--corpo della pagina-->
  <div class="w3-container stringi" style=" margin-top: 80px; min-height: 67.9%;">

<form id="form" action="welcome.php" method="post">
    <div class="w3-row rimpicciolisci rimpiccioliscixxl" >
          <strong><label for="email">Email</label></strong>
          <input id="email" type="text" for="emaillog" placeholder="Inserisci l'Email" name="email" required>

          <strong><label for="psw">Password</label></strong>
          <input id="psw" type="password" for="pswlog" placeholder="Inserisi Password" name="psw" required>

          <strong><label id="repeat1" style="display:none;" for="repeat2">Ripeti Password</label></strong>
          <input id="repeat2" style="display:none;" for="psw-repeat" type="password" placeholder="Ripeti Password" name="pswrepeat"  >


          <p id="privacy" style="display:none;">Creando questo account accetterai i seguenti <a href="#" style="color:dodgerblue">Termini & Privacy</a>.</p>

          <div class="clearfix">
              <button id="btnaccedi" class="buttonform signupbtn" style="text-align:center;" type="submit" name="accedi" >Accedi</button>
              <button id="btnnotregister" class="buttonform cancelbtn" style="text-align:center;" type="button " onclick="notregister()">Registrazione</button>
              <button id="btnregistrati" class="buttonform signupbtn" style="width:100%;display:none; text-align:center;" name="registrati" type="submit">Registrati</button>

          </div>
    </div>

</form>

  </div>



<!-- riga finale -->
<footer class= "w3-padding-16 " style=" position:absolute; bottom:0; width:100%; background-color: lightgray">
  <div class="w3-row">
  <div class="w3-third w3-container" >
    <a target="_blank" href=""><p class="sposta w3-left ">&nbsp;Privacy policy&nbsp;</p ></a>
    <a target="_blank" href=""><p class="sposta w3-left ">Cookie policy </p></a>
  </div>
  <div class="w3-third w3-container ">
    <p class="w3-center">P.IVA - C.F 01234567890</p>
  </div>
  <div class="w3-third w3-container">
    <p class="w3-right sposta">Copyright © 2018 New tastes&nbsp;&nbsp;</p>
  </div>
</div>
</footer>

</body>
</html>
