<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html  xml:lang="it" lang="it">
<title>New tastes</title>
<link rel="icon" href="immagini/favicon.ico" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="login.js"></script>
<script type="text/javascript" src="admin.js"></script>

<style type="text/css">@import url(style.css);</style>
<style type="text/css">@import url(login.css);</style>
<style type="text/css">@import url(admin.css);</style>

<?php session_start();?>

<body>
  <div class="w3-top">
    <!-- barra di navigazione del sito -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="sito.php">New tastes</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" >
      <div id="simb" class="simbolo" onclick="Simbolo()">
      <div class="bar1"></div>
      <div class="bar2"></div>
      <div class="bar3"></div>
      </div>
    </button>
    <div class="collapse navbar-collapse  " id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" onclick="Simbolo()" href="sito.php">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" onclick="Simbolo()" href="sito.php#about">Chi siamo</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" onclick="Simbolo()" href="sito.php#panini">Panini</a>
        </li>
        <li class="nav-item">
          <a class="nav-link"  onclick="Simbolo()" href="sito.php#contact">Contatti</a>
        </li>

        <?php
        if(isset($_SESSION["login"]) ){
            if($_SESSION["login"]== true )
            {
            echo"<li class='nav-item'>
                  <a  id='logout' class='nav-link' style='' onclick='chiusura(2);' name='log' >Logout</a>
                </li>";
              }
            }
              ?>
      </ul>
    </div>
    </nav>
  </div>


    <div class="w3-container stringi " style="margin-top:80px; min-height: 67.9%;">
    <div id="divord" style="" class="rimpicciolisci rimpiccioliscixxl">
      <button id="btnback4" class="buttonform signupbtn" style=" width:100%;background-color: lightgray; text-align:center; border-radius: 10px;"><a href="admin.php">back</a></button>

        <div id="divordin" style="" class="">
          <?php
          $servername = "localhost";
          $username = "matteo";
          $password = "123456";
          $dbname = "paninoloco";


          // Create connection
          $conn = new mysqli($servername, $username, $password, $dbname);
          // Check connection
          if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
          }

          $sql = "SELECT Nome, Prezzo, Panini, Orario, Indirizzo  FROM ordini WHERE Stato='Attivo'";
          $result = $conn->query($sql);
          $count = 1;


          while($row = $result->fetch_assoc())/*controllo fino a quando ho valori dentro a result*/
          {
            $orario = $row["Orario"];
           echo"

                <div class='w3-rest' style='border: lightgray 1px solid'>
                      <div class='w3-row w3-border-light-grey' >
                        <div class='w3-twothird  w3-padding' style='width:50%;'>
                            <p>$row[Nome]</p>
                        </div>
                        <div class='w3-third w3-padding' style='width:50%; text-align:right;  '>
                            <p>$row[Orario]</p>
                        </div>
                      </div>

                      <div class='' style='margin-left:10px; width:50%;' >
                          <p>$row[Panini]</p>
                      </div>

                      <div class='w3-row' style='margin-left:10px;' >
                          <div class='w3-half' style='margin-top:20px; width:50%;'>
                              <p>$row[Indirizzo]</p>
                          </div>
                          <div class='w3-half' style='width:50%;'>
                              <button  class='buttonform signupbtn' style='width:90%; text-align:center; border-radius: 10px;' type='button'  onclick='rigaelim(\"".$orario."\");'>Pronto</button>
                          </div>
                      </div>
                    </div>
              <p style='height:5px;'></p>
              ";

              $count++;
          }
          ?>



        </div>
    </div>
    </div>


    <footer class= "w3-Grey w3-padding-16 " style="background-color: lightgray">
      <div class="w3-row">
      <div class="w3-third w3-container" >
        <a target="_blank" href=""><p class="sposta w3-left ">&nbsp;Privacy policy&nbsp;</p ></a>
        <a target="_blank" href=""><p class="sposta w3-left ">Cookie policy </p></a>
      </div>
      <div class="w3-third w3-container ">
        <p class="w3-center">P.IVA - C.F 01234567890</p>
      </div>
      <div class="w3-third w3-container">
        <p class="w3-right sposta">Copyright © 2018 New tastes&nbsp;&nbsp;</p>
      </div>
    </div>
    </footer>

    </body>
    </html>
