function vaipagamento(){
  var menu =$("#menu");
  var datiordine=$("#datiordine");

  menu.css("display","none");
  datiordine.css("display","block");
}


/*controlli da eseguire sui dati per il pagamento*/
function controlli(){
  var errore=0;
  var nome = document.getElementById("nome").value;                    //
  var indirizzo = document.getElementById("indirizzo").value;
  var ora = document.getElementById("ora").value;                      //
  var minuto = document.getElementById("Minuto").value;                //
  var titolarecarta = document.getElementById("titolarecarta").value;  //
  var ncarta = document.getElementById("ncarta").value;                //
  var meseselezionato = $('#mesi').val();                              //
  var annoscadenza = document.getElementById("annoscadenza").value;    //
  var cvv = document.getElementById("cvv").value;                      //

  /*controllo sul codice segreto della carta*/
  if(cvv.length != 3){
      errore++;
      alert("CVV non volido");
  }

  for(i=0;i<cvv.length;i++){
       if(cvv[i] >= 'a' && cvv[i] <= 'z')
       {
         errore++;
         alert("CVV non valido");
         break;
       }
    }

  /*controllo sull'anno di scadenza della crta */
    if (annoscadenza < 2018){
      errore++;
      alert("Carta scaduta");
    }
    if (annoscadenza > 2023 ){
      errore++;
      alert("Anno scadenza non valido");
    }
    for(i=0;i<annoscadenza.length;i++){
         if(annoscadenza[i] >= 'a' && annoscadenza[i] <= 'z')
         {
             errore++;
           alert("Anno di scadenza non valido");

           break;
         }
      }

   /*controllo sul numero della carta*/
    if(ncarta.length !=16){
      if(ncarta.length !=13){
        alert("numero carta non valido");
         errore++;
      }
    }
    for(i=0;i<ncarta.length;i++){
         if(ncarta[i] >= 'a' && ncarta[i] <= 'z')
         {
           alert("Numero carta non valido");
            errore++;
           break;
         }
      }

   /*ora e minuti attuali*/
   var data = new Date();
    var h =data.getHours();
    var m =data.getMinutes();
    var a = data.getFullYear();

  /*controllo sull'orario inserito*/
    if(ora <8 || ora >21){
      alert("La paninoteca è chiusa nell'orario inserito");
        errore++;
    }
    else{
      if(h > ora )
      {
         alert("L'orario è già passato");
        errore++;
      }
      else{

        if(h == ora && m>40)
        {
                alert("L'ordine deve essere fatto almeno venti minuti prima");
                errore++;
        }else{
          if(h == ora && m<40)
          {
            if( m+20 > minuto)
            {
                  alert("L'ordine deve essere fatto almeno venti minuti prima");
                   errore++;
            }
          }
        }


          if(h+1 == ora && m >= 40 )
          {
            if( m-40 > minuto)
            {
                  alert("L'ordine deve essere fatto almeno venti minuti prima");
                   errore++;
            }
          }

     }
}
    for(i=0;i<ora.length;i++){
         if(ora[i] >= 'a' && ora[i] <= 'z')
         {
           alert("Orario non valido");
            errore++;
           break;
         }
      }

    if(minuto < 0 || minuto >59 ){
        alert("I minuti non sono stati inseriti correttamente");
        errore++;
    }
    for(i=0;i<minuto.length;i++){
         if(minuto[i] >= 'a' && minuto[i] <= 'z')
         {
           alert("Orario non valido");
            errore++;
           break;
         }
      }


  /*controllo sul titolare della carta*/
      for(i=0;i<titolarecarta.length;i++){
           if(titolarecarta[i] >= '0' && titolarecarta[i] <= '9')
           {
             alert("Titolare della carta non valido");
            errore++;
             break;
           }
        }

  /*controllo sul Nominativo dell ordine*/
        for(i=0;i<nome.length;i++){
             if(nome[i] >= '0' && nome[i] <= '9')
             {
               alert("Nominativo dell'ordine non valido");
                errore++;
               break;
             }
          }

  /*controllo su mese selezionato*/
      var mese = data.getMonth();
      if(annoscadenza == a && meseselezionato < mese ){
       alert("Carta scaduta");
       errore++;
     }


   if(errore==0){

     var xmlhttp = new XMLHttpRequest();

     xmlhttp.onreadystatechange = function() {
     if (this.readyState == 4 && this.status == 200) {
     location.href="sito.php";
     }
   };
   xmlhttp.open("POST","pagamento.php",true);
   xmlhttp.send($("#pagamento").serialize());
   }

  errore=0;

}
