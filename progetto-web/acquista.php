<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html  xml:lang="it" lang="it">
<title>New tastes</title>
<link rel="icon" href="immagini/favicon.ico" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="login.js"></script>
<script type="text/javascript" src="acquista.js"></script>


<style type="text/css">@import url(style.css);</style>
<style type="text/css">@import url(login.css);</style>
<style type="text/css">@import url(acquista.css);</style>


<?php session_start();?>

<body>
  <div class="w3-top">
    <!-- barra di navigazione del sito -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="sito.php">New tastes</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" >
      <div id="simb" class="simbolo" onclick="Simbolo()">
      <div class="bar1"></div>
      <div class="bar2"></div>
      <div class="bar3"></div>
      </div>
    </button>
    <div class="collapse navbar-collapse  " id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" onclick="Simbolo()" href="sito.php">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" onclick="Simbolo()" href="sito.php#about">Chi siamo</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" onclick="Simbolo()" href="sito.php#panini">Panini</a>
        </li>
        <li class="nav-item">
          <a class="nav-link"  onclick="Simbolo()" href="sito.php#contact">Contatti</a>
        </li>
      </ul>
    </div>
    </nav>
  </div>


  <?php
  if(isset($_SESSION["msg"]) == TRUE){
      if($_SESSION["msg"]!=""){
        echo"<div id='msgerrore' class='finestralogout' style='display:block;'>
            <!-- Modal content -->
            <div class='modal-content'>
              <span class='close' onclick='chiusuraF(2)'>&times;</span>
              <p>".$_SESSION["msg"]."</p>
            </div>
          </div>";
      }
      $_SESSION["msg"]="";
  }
  ?>

<!--corpo della pagina-->
  <div class="w3-container " style=" margin-top: 80px; min-height: 67.9%;">

          <div id="menu" class="w3-padding ">
            <h3>Menu</h3>
            <p style=" height: 5px;"></p>
            <?php
                $servername = "localhost";
                $username = "matteo";
                $password = "123456";
                $dbname = "paninoloco";


                // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);

                $sql = "SELECT Nome, Prezzo  FROM prodotti";
                $result = $conn->query($sql);

                while($row = $result->fetch_assoc())/*controllo fino a quando ho valori dentro a result*/
                {
                    echo"

                    <div class=' w3-row-padding ' >

                      <div class=' w3-col w3-margin-bottom' style='width:60%' >
                            <p>".$row["Nome"]."</p>
                      </div>
                      <div  class=' w3-col w3-margin-bottom' style='width:20%'>
                            <p id='prezzo' style='float:right;'>".$row["Prezzo"]." €</p>
                      </div>
                      <div  class=' w3-col w3-margin-bottom' style='width:20%'>
                            <button class='btnplus' value='".$row["Nome"]."' name='btn'>+</button>
                      </div>
                  </div>";
                }
                echo"

                <div class=' w3-row-padding ' >

                  <div class=' w3-col w3-margin-bottom' style='width:60%' >
                        <p>Consegna</p>
                  </div>
                  <div  class=' w3-col w3-margin-bottom' style='width:20%'>
                        <p id='prezzo' style='float:right;'>+2 €</p>
                  </div>
                  <div  class=' w3-col w3-margin-bottom' style='width:20%'>

                  </div>
              </div>";

                  echo"<script>
                  $('.btnplus').click(function(){

                    var btn = $('#btnpaga');
                    btn.css('display','block');

                  var xmlhttp = new XMLHttpRequest();
                  xmlhttp.open('POST', 'ordinazione.php',true);
                  xmlhttp.send($(this).val());

                    xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                      document.getElementById('prezzotot').innerHTML = this.responseText;
                    }
                  };
                 }
                  );

                  </script>";
                  $conn->close();
            ?>

                <div class="w3-row-padding" >
                    <div class="w3-col w3-margin-bottom" style="width:45%">
                      <button id="btnpaga" style="display:none; border-radius: 10px;" onclick="vaipagamento()">Paga</button>
                  </div>
                  <div class="w3-col w3-margin-bottom" style="width:40%">
                      <p id="prezzotot" style='float: right; margin-right:30px;'>TOT: 0 €</p>
                  </div>
                  <div class="w3-col w3-margin-bottom" style="width:35%">

                  </div>
               </div>
      </div>



    <div id="datiordine" style="display:none;" class=" w3-padding"  >

             <form id="pagamento">

      <div class="w3-half w3-padding">
                  <h3 class="w3-center">Dati per la consegna</h3>
                  <p style=" height: 18px;"></p>
                  <strong><label for="nome">Nome</label></strong>
                  <input type="text" id="nome" placeholder="Nome" name="Nome" required>

                  <strong><label for="indirizzo">indirizzo</label></strong>
                  <input type="text" id="indirizzo" placeholder="Inserisci il tuo indirizzo" name="Indirizzo" required>

                  <p style=" height: 10px;"></p>

                  <strong><label for="ora" for="Minuto">Orario Consegna</label></strong>
                  <div class="row">
                    <div class="col">
                      <input id="ora" type="text"  name="ora" placeholder="ora" title="ora" required>
                    </div>
                    <div class="col">
                      <input id="Minuto" type="text" name="minuti" placeholder="Minuto" title="minuto" required>
                    </div>
                  </div>

                  <p style=" height: 20px;"></p>
       </div>

              <div class="w3-half w3-padding">

                  <h3 class="w3-center">Pagamento</h3>
                        <p style=" height: 18px;"></p>

                             <strong><label for="titolarecarta">Titolare della carta</label></strong>
                             <input type="text" id="titolarecarta" name="titolarecarta" placeholder="Matteo Suzzi" required>
                             <strong><label for="ncarta">Numero carta di credito</label></strong>
                             <input type="text" id="ncarta" name="ncarta" placeholder="1111-2222-3333-4444" required>

                             <strong><label for="mesi">Mese di scadenza</label></strong>
                             <div class="select-style" >
                              <select id="mesi" class="w3-padding" style="width:100%; border: 1px solid #ccc; border-radius: 3px;">
                                   <option value="0">Gennaio</option>
                                   <option value="1">Febbraio</option>
                                   <option value="2">Marzo</option>
                                   <option value="3">Aprile</option>
                                   <option value="4">Maggio</option>
                                   <option value="5">Giugno</option>
                                   <option value="6">Luglio</option>
                                   <option value="7">Agosto</option>
                                   <option value="8">Settembre</option>
                                   <option value="9">Ottobre</option>
                                   <option value="10">Novembre</option>
                                   <option value="11">Dicembre</option>
                             </select>
                           </div>

                          <p style=" height: 8px;"></p>

                             <div class="row">
                               <div class="col">
                                 <strong><label class="w3-center" for="annoscadenza" style="font-weight: 500;">Anno di scadenza</label></strong>
                                 <input type="text" id="annoscadenza" name="annoscadenza" placeholder="2018" required>
                               </div>
                               <div class="col">
                                 <strong><label class="w3-center" for="cvv">CVV</label></strong>
                                 <input type="text" id="cvv" name="cvv" placeholder="352" required>
                               </div>
                             </div>
                      </div>


            </form>
            <div style="text-align:center;">
                <button class="" name="paga" onclick="controlli()">Paga Ordine</button>
            </div>
            </div>

  <p style=" height: 21px;"></p>
    </div>



<!-- riga finale -->
<footer class= "w3-Grey w3-padding-16 "style="background-color:lightgray">
  <div class="w3-row">
  <div class="w3-third w3-container" >
    <a target="_blank" href=""><p class="sposta w3-left ">&nbsp;Privacy policy&nbsp;</p ></a>
    <a target="_blank" href=""><p class="sposta w3-left ">Cookie policy </p></a>
  </div>
  <div class="w3-third w3-container ">
    <p class="w3-center">P.IVA - C.F 01234567890</p>
  </div>
  <div class="w3-third w3-container">
    <p class="w3-right sposta">Copyright © 2018 New tastes&nbsp;&nbsp;</p>
  </div>
</div>
</footer>
</div>

</body>
</html>
