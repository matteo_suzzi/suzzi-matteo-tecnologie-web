<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html  xml:lang="it" lang="it">
<title>New tastes</title>
<link rel="icon" href="immagini/favicon.ico" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style type="text/css">@import url(style.css);</style>
<style type="text/css">@import url(login.css);</style>

<?php session_start();?>

<body>
  <div  class="w3-top">
    <!-- barra di navigazione del sito -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="sito.php">New tastes</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" >
      <div id="simb" class="simbolo" onclick="Simbolo()">
      <div class="bar1"></div>
      <div class="bar2"></div>
      <div class="bar3"></div>
      </div>
    </button>
    <div class="collapse navbar-collapse  " id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" onclick="Simbolo()" href="#about">Chi siamo</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" onclick="Simbolo()" href="#panini">Panini</a>
        </li>
        <li class="nav-item">
          <a class="nav-link"  onclick="Simbolo()" href="#contact">Contatti</a>
        </li>
        <?php
        if(isset($_SESSION["login"]) ){
            if($_SESSION["login"]== true )
            {
            echo"<li class='nav-item'>
                  <a id='logout' class='nav-link' onclick='chiusura(1)' name='log'>Logout</a>
                </li>
                <li class='nav-item'>
                  <a id='acquisto' class='nav-link' onclick='Simbolo()' href='acquista.php'>Acquista</a>
                </li>";

            }else{
              echo"<li class='nav-item'>
                  <a  id='Login1' class='nav-link'  onclick='Simbolo()' href='login.php'>Login</a>
                </li>";
            }
        }else{
            echo"<li class='nav-item'>
              <a  id='Login2' class='nav-link'  onclick='Simbolo()' href='login.php'>Login</a>
            </li>";
        }

        ?>

      </ul>
    </div>
    </nav>
  </div>





  <!-- The Modal -->
  <div id="msglogout" class="finestralogout" style="display:none;">
    <!-- Modal content -->
    <div class="modal-content">
      <span class="close" onclick="chiusuraF(1)">&times;</span>
      <p>Il logout è stato eseguito correttamente</p>
    </div>
  </div>






  <div class="casella abbassa">
   <img alt="immagine principale sito" style="width:100%" src="immagini/testata.jpg" />
</div>


<!--corpo della pagina-->
  <div class="w3-container " style=" margin-bottom:0; min-height: 80.5%;">

  <!-- informazioni sull'azienda -->
  <div class="w3-container w3-padding-32 w3-mobile" id="about">
    <h3 class="w3-border-bottom w3-border-light-grey w3-padding-16">Paninoteca</h3>

    <div class="w3-half w3-center centra">
     <img alt="immagine paninoteca" src="immagini/paninoteca.jpg" class="w3-round w3-center" style="max-width:100%">
    </div>

    <div class="w3-half centra">
      <h2 class="w3-center">New tastes</h2><br>
      <p class="w3-large" style="margin-left:20px; margin-right:10px;"> La paninoteca è nata a Cesena nel 2008 dalla collaborazione con una Azienda agricola biologica
                                                                        Suzzi Sergio & C.
                                                                        Gli ingredienti dei nostri panini provengono esclusivamente da aziende biologiche della zona , per offrire
                                                                        ai nostri clienti cibo sano e di alta qualità. La paninoteca consegna in tutta la zona di Cesena tutti i girni dalle ore
                                                                        8:00 alle 21:00, con un piccolo contributo di 2€ per la consegna a casa.
                                                                        </p>
    </div>
  </div>

  <!-- realizzazioni -->
  <div style="margin-bottom:10px;">
    <div class="w3-col s6">
        <h3>I nostri panini</h3>
    </div>
    <div class="w3-col s6">
      <?php
      if(isset($_SESSION["login"]) == TRUE){
          if($_SESSION["login"] == "true")
          {
            echo"<h3 id='btnacquista' style='float:right;' ><a href='acquista.php' >Acquista</a></h3>";
          }
        }
        ?>
    </div>
  </div>



  <div id="panini" class="w3-border-bottom w3-border-light-gray w3-padding-16"></div>

<p style=" height: 5px;"></p>

    <div class="w3-row-padding w3-center">

                          <div class="w3-col l2 m4 s6 w3-margin-bottom" >
                            <div class="card ">
                              <div class="realizzazione">
                                <a href="#myDIV" onclick="myFunction(1,'immagini/prodotti/1.jpg','Pane','Insalata','Carne di manzo','Bacon','Cheddar','Pomodoro')"><h4><strong class="centred">Bacon Cheeseburger</strong></h4></a>
                              </div>
                            </div>
                          </div>

                          <div class="w3-col l2 m4 s6 w3-margin-bottom">
                            <div class="card">
                              <div class="realizzazione">
                                <a href="#myDIV" onclick="myFunction(2,'immagini/prodotti/2.jpg','Pane','Pomodoro','Lattuga','Bacon','Maionese','Tacchino')"><h4><strong class="centred">Club Sandwich</strong></h4></a>
                              </div>
                            </div>
                          </div>

                          <div class="w3-col l2 m4 s6 w3-margin-bottom">
                            <div class="card">
                              <div class="realizzazione">
                              <a href="#myDIV" onclick="myFunction(3,'immagini/prodotti/3.jpg','Pane','Salsiccia','cipolla','Peperoni','senape','Salsa Panino Loco')" ><h4><strong class="centred">Hot Dog</strong></h4></a>
                              </div>
                            </div>
                          </div>

                          <div class="w3-col l2 m4 s6 w3-margin-bottom">
                            <div class="card">
                              <div class="realizzazione">
                               <a href="#myDIV" onclick="myFunction(4,'immagini/prodotti/4.jpg','Pane','Carne di agnello','Cipolla','Patate fritte','Pomodoro','Salsa jogurt')" ><h4><strong class="centred">Kebab</strong></h4></a>
                              </div>
                            </div>
                          </div>

                          <div class="w3-col l2 m4 s6 w3-margin-bottom">
                            <div class="card">
                              <div class="realizzazione">
                                <a href="#myDIV" onclick="myFunction(5,'immagini/prodotti/5.jpg','Pane','Burger di ceci','Pomodoro','Cetriolo','Crema di rapa','Germogli di soia')" ><h4><strong class="centred">Veggie burger</strong></h4></a>
                              </div>
                            </div>
                          </div>

                          <div class="w3-col l2 m4 s6 w3-margin-bottom">
                            <div class="card">
                                <div class="realizzazione">
                                <a href="#myDIV" onclick="myFunction(6,'immagini/prodotti/6.jpg','Pane','Pollo','Insalata','Pomodoro','Cipolla','Maionese')" ><h4><strong class="centred">Hamburger di pollo</strong></h4></a>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div id="myDIV" class="w3-row "style="margin-bottom:10px; display:none; border-radius: 10px 10px 10px 10px;">
                            <div id="half1" class="w3-half w3-center centra"></div>
                            <div id="half2" class="w3-half w3-center centra"></div>
                        </div>

</div>
<p style=" height: 50px;"></p>


  <!-- contatti -->
  <div class="w3-container w3-content w3-padding-64 w3-mobile" style="max-width:1500px" id="contact">
    <h2 class="w3-wide w3-center">CONTATTACI</h2>
    <div class="w3-row w3-padding-32">
      <div class="w3-half w3-large w3-center centra">
        <em class="fa fa-map-marker" style="width:30px"></em><a target="_blank" href="https://www.google.it/maps/search/Via+Paolo+Veronese,+Cesena,+FC/@44.1513144,12.2576908,18z">Via Paolo Veronese Cesena,FC</a><br>
        <em class="fa fa-phone" style="width:30px"></em><a href="tel:3453505850">Telefono: +39 3453505850</a><br>
        <em class="fa fa-envelope" style="width:30px"></em><a href="mailto:suzzi.matteo@gmail.com">Email: suzzi.matteo@gmail.com</a><br>

      </div>
      <div class="w3-half w3-center centra abbassa"  >
        <iframe class="ridimensionaM" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1431.349656768722!2d12.257610379431082!3d44.15144407987317!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132cbb2a6982b39b%3A0xc6ab10ca277eb789!2sVia+Paolo+Veronese%2C+135%2C+47521+Cesena+FC!5e0!3m2!1sit!2sit!4v1527491281212" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
    <div class=" w3-padding-64 w3-center w3-opacity w3-xxxlarge">
  <a target="_blank" href="https://www.facebook.com/matteo.suzzi.90"><em class="fa fa-facebook-official w3-hover-opacity"></em></a>
  <a target="_blank" href ="https://www.instagram.com/matteo_suzzi/?hl=it"><em class="fa fa-instagram w3-hover-opacity"></em></a>
  </div>
</div>




<div class="footer">
<!-- riga finale -->
<footer class= "w3-Grey w3-padding-16 " style="background-color: lightgray">
  <div class="w3-row">
  <div class="w3-third w3-container " >
    <a target="_blank" href=""><p class="sposta w3-left">&nbsp;Privacy policy&nbsp;</p ></a>
    <a target="_blank" href=""><p class="sposta w3-left">Cookie policy </p></a>
  </div>
  <div class="w3-third w3-container ">
    <p class="w3-center">P.IVA - C.F 01234567890</p>
  </div>
  <div class="w3-third w3-container">
    <p class="w3-right sposta">Copyright © 2018 New tastes&nbsp;&nbsp;</p>
  </div>
</div>
</footer>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="login.js"></script>

</body>
</html>
